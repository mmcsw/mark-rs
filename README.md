# mark-rs

## what?

An OpenGL-accelerated markdown editor written in Rust and inspired by Alacritty / Typora.
This is in active development, but here is a list of features I plan to implement at some point:

* rope-based text engine
* Novel markdown to HTML/PDF processing 
* Separate frontend/backend so that you can generate HTML/PDFs using the shell.
* OpenGL hardware acceleration (obviously)
* Plugin engine using Lua
* Plugin manager a la Sublime's Package Control.

## why?

Typora is large (>200MB last time I checked and with a 150MB RAM footprint), closed-source, and will be paid when the beta ends.
I think this is unacceptable for what amounts to a glorified text editor.

## credits

`mark-rs` would not happen without other Rust ecosystem libraries.
Here's what I use:

* [`ropey`](https://github.com/cessen/ropey)
* [`glutin`](https://github.com/rust-windowing/glutin)
* [`gl_bindings`](:w)
* [`rlua`](https://github.com/amethyst/rlua)

And of course thank you to the many contributors of [`alacritty`](https://github.com/alacritty/alacritty/tree/master/alacritty/src) for their work, which has proven invaluable inspiration for the code here.

## other work

browse here or go to [xi9.io](https://xi9.io)
