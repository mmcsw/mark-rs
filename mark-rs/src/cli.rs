
// any options that can be passed on the command-line are wrapped up in this struct.
pub struct Options {
    config_path: Option<std::path::PathBuf>,
    config_options: Option<()>, // TODO: what is a good yaml value, from serde?
    log_location: Option<std::path::PathBuf>,
    log_level: log::LevelFilter,
    log_persistent: bool, 
    
}
